#### Description
This is a simple bulleting board. Logged in users can post content which are immediately shared with all other logged in users.

#### How to run in local?
1. Clone the repo
2. `cd` into it
3. `yarn install` (or `npm install`)
4. `yarn start` (or `npm start`)

#### Live URI
https://bulletin-board-7de92.firebaseapp.com/
