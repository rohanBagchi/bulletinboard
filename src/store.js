import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './rootReducer.js';

const initialState = {};
const middleWares = [];

const composeEnhancers = process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({

      }) : compose;

const composedEnhancers = composeEnhancers(
    applyMiddleware(...middleWares)
);

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

export default store;
