import _ from 'underscore';

export function getPostIdsFromReadList(read_list) {
    const post_ids_in_read_list = _.keys(read_list).map(key => read_list[key]);
    return _.pluck(post_ids_in_read_list, 'post_id');
};

export function isPostInReadList(post_id, read_list) {
    const post_ids_in_read_list = getPostIdsFromReadList(read_list);
    return post_ids_in_read_list.filter(p_id => p_id === post_id).length;
}
