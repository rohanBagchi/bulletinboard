import {createAction} from 'redux-actions';
import keyMirror from 'keymirror';
import _ from 'underscore';
import {database} from './index.js';
import {isPostInReadList} from './utils/PostsUtils.js';

export const PostsActionTypes = keyMirror({
	INIT: null,
	SET_POSTS: null,
    SET_IS_LOADING: null,
    SET_NEW_POST_FORM_VISIBILITY: null,
    RESET_NEW_POST: null,
    SYNC_NEW_POST_TITLE: null,
    SYNC_NEW_POST_CONTENT: null,
    SAVE_NEW_POST: null,
    TOGGLE_POST_EXPAND: null,
    SET_USER_READ_LIST: null,
    UPDATE_USER_READ_LIST: null,
    DELETE_POST: null
});

export const initializePosts = createAction(
	PostsActionTypes.INIT,
	dispatch =>
        dispatch(setIsLoading(true))
);

export const setPosts = createAction(
    PostsActionTypes.SET_POSTS,
    posts => {
        const keys = _.keys(posts);
        return keys.map(key => ({
            ...posts[key],
            _id: key,
            is_expanded: false
        }));
    }
);

export const syncNewPostTitle = createAction(
    PostsActionTypes.SYNC_NEW_POST_TITLE,
    title => title
);

export const syncNewPostContent = createAction(
    PostsActionTypes.SYNC_NEW_POST_CONTENT,
    content => content
);

export const setIsLoading = createAction(
    PostsActionTypes.SET_IS_LOADING,
    is_loading => is_loading
);

export const setNewPostFormVisibility = createAction(
    PostsActionTypes.SET_NEW_POST_FORM_VISIBILITY,
    is_visible => is_visible
);

export const resetNewPostForm = createAction(
    PostsActionTypes.RESET_NEW_POST,
    () => ({})
);

export const saveNewPost = createAction(
    PostsActionTypes.SAVE_NEW_POST,
    (dispatch, {title, content}, user_email, user_uid) => {
        if (content.length > 100) {
            content = content.substring(0, 101);
        }
        const new_post = {
            created_by: user_email,
            title: title,
            content: content
        }
        const newPostKey = database.ref().child('posts').push().key;
        const updates = {};
        updates[`/posts/${newPostKey}`] = new_post;
        const update_op = database.ref().update(updates);
        update_op.then(data => {
            dispatch(resetNewPostForm());
            dispatch(updateUserReadList({
                user_uid,
                post_id: newPostKey
            }));
        });
    }
);

export const togglePostExpand = createAction(
    PostsActionTypes.TOGGLE_POST_EXPAND,
    (dispatch, {user_uid, post_id, user_read_list}) => {
        if (user_uid && !isPostInReadList(post_id, user_read_list)) {
            dispatch(updateUserReadList({
                user_uid,
                post_id
            }));
        }
        return post_id;
    }
);

export const setUserReadList = createAction(
    PostsActionTypes.SET_USER_READ_LIST,
    read_list => read_list
);

export const updateUserReadList = createAction(
    PostsActionTypes.UPDATE_USER_READ_LIST,
    ({user_uid, post_id}) => {
        const read_list_item = {
            post_id
        };
        const readListItemKey = database.ref().child(`read_list/${user_uid}`).push().key;
        const updates = {};
        updates[`/read_list/${user_uid}/${readListItemKey}`] = read_list_item;
        database.ref().update(updates);
    }
);

export const deletePost = createAction(
    PostsActionTypes.DELETE_POST,
    (post, requesting_user_email) => {
        if (post.created_by === requesting_user_email) {
            database.ref(`posts/${post._id}`).remove();
        }
    }
);

const default_new_post_form = {
    is_visible: false,
    title: '',
    content: ''
};
const default_state = {
    is_loading: false,
    new_post_form: default_new_post_form,
    posts: [],
    user_read_list: []
};

export default function reducer(state, action) {
	state = state || default_state;
    let posts = state.posts;
	switch(action.type) {
		case PostsActionTypes.SET_POSTS:
            return {
                ...state,
                posts: action.payload
            };

        case PostsActionTypes.SET_IS_LOADING:
			return {
                ...state,
                is_loading: action.payload
            };

        case PostsActionTypes.SYNC_NEW_POST_TITLE:
            return {
                ...state,
                new_post_form: {
                    ...state.new_post_form,
                    title: action.payload
                }
            };

        case PostsActionTypes.SYNC_NEW_POST_CONTENT:
            return {
                ...state,
                new_post_form: {
                    ...state.new_post_form,
                    content: action.payload
                }
            };

        case PostsActionTypes.SET_NEW_POST_FORM_VISIBILITY:
            return {
                ...state,
                new_post_form: {
                    ...state.new_post_form,
                    is_visible: action.payload
                }
            };

        case PostsActionTypes.RESET_NEW_POST:
            return {
                ...state,
                new_post_form: {
                    ...default_new_post_form
                }
            };

        case PostsActionTypes.TOGGLE_POST_EXPAND:
            posts = posts.map(post => {
                if (post._id === action.payload) {
                    return {
                        ...post,
                        is_expanded: !post.is_expanded
                    };
                }
                return post;
            });
            return {
                ...state,
                posts
            };

        case PostsActionTypes.SET_USER_READ_LIST:
            return {
                ...state,
                user_read_list: action.payload
            };

		default:
			return state;
	}
}
