import {connect} from 'react-redux';
import UnreadItemsCount from '../components/UnreadItemsCount.js';
import _ from 'underscore';

function mapStateToProps(state) {
    return {
        current_user: state.users_reducer.current_user,
        posts: state.posts_reducer.posts,
        user_read_list: state.posts_reducer.user_read_list
    };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
    const posts = stateProps.posts;
    const user_uid = stateProps.current_user ? stateProps.current_user.uid : null;
    const user_read_list = stateProps.user_read_list[user_uid];
    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        post_ids: _.pluck(posts, '_id'),
        user_read_list
    };
}

export default connect(
    mapStateToProps,
    null,
    mergeProps
)(UnreadItemsCount);

