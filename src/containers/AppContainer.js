import {connect} from 'react-redux';
import App from '../components/App.js';
import {initializePosts,
    setNewPostFormVisibility,
    syncNewPostTitle,
    syncNewPostContent,
    togglePostExpand,
    deletePost} from '../PostsDuck.js';

function mapStateToProps(state) {
    return {
        current_user: state.users_reducer.current_user,
        is_logged_in: state.users_reducer.is_current_user_logged_in,
        posts: state.posts_reducer.posts,
        posts_is_loading: state.posts_reducer.is_loading,
    	user_read_list: state.posts_reducer.user_read_list,
        new_post_form: state.posts_reducer.new_post_form
    };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
    const {dispatch} = dispatchProps;
    const user_email = stateProps.current_user ? stateProps.current_user.email : null;
    const user_uid = stateProps.current_user ? stateProps.current_user.uid : null;
    const user_read_list = stateProps.user_read_list[user_uid];
    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        user_read_list,
        initializePosts: () =>
            dispatch(initializePosts(dispatch)),
        setNewPostFormVisibility: (is_visible) =>
            dispatch(setNewPostFormVisibility(is_visible)),
        syncNewPostTitle: (title) =>
            dispatch(syncNewPostTitle(title)),
        syncNewPostContent: (content) =>
            dispatch(syncNewPostContent(content)),
        togglePostExpand: (post_id) =>
            dispatch(togglePostExpand(dispatch, {user_uid, post_id, user_read_list})),
        deletePost: (post) =>
            dispatch(deletePost(post, user_email))
    }
}

export default connect(
    mapStateToProps,
    null,
    mergeProps
)(App);

