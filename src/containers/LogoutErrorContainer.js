import {connect} from 'react-redux';
import LogoutError from '../components/LogoutError.js';

function mapStateToProps(state) {
    return {
        has_logout_error: state.users_reducer.has_logout_error,
        logout_errors: state.users_reducer.logout_errors
    };
}

function mapDispatchToProps(dispatch) {
    return {
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LogoutError);

