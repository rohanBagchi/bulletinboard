import {connect} from 'react-redux';
import Login from '../components/Login.js';
import {syncLoginEmail, syncLoginPassword, attemptLogin} from '../UsersDuck.js';

function mapStateToProps(state) {
    return {
        users: state.users_reducer.users,
        email: state.users_reducer.login_form.email,
        password: state.users_reducer.login_form.password,
        has_error: state.users_reducer.login_form.has_error,
        errors: state.users_reducer.login_form.errors
    };
}

function mapDispatchToProps(dispatch) {
    return {
        syncLoginEmail: (email) =>
            dispatch(syncLoginEmail(email)),
        syncLoginPassword: (password) =>
            dispatch(syncLoginPassword(password)),
        attemptLogin: ({email, password}) =>
            dispatch(attemptLogin(dispatch, {email, password}))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);

