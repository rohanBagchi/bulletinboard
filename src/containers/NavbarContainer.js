import {connect} from 'react-redux';
import Navbar from '../components/Navbar.js';
import {attemptLogout} from '../UsersDuck.js';

function mapStateToProps(state) {
    return {
        is_logged_in: state.users_reducer.is_current_user_logged_in
    };
}

function mapDispatchToProps(dispatch) {
    return {
        attemptLogout: () => dispatch(attemptLogout(dispatch))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navbar);

