import {connect} from 'react-redux';
import PostForm from '../components/posts/PostForm.js';
import {syncNewPostTitle, syncNewPostContent, saveNewPost, setNewPostFormVisibility} from '../PostsDuck.js';

function mapStateToProps(state) {
    return {
        current_user: state.users_reducer.current_user,
        new_post_form: state.posts_reducer.new_post_form
    };
}

function mergeProps(stateProps, dispatchProps, ownProps) {
    const {dispatch} = dispatchProps;
    const user_uid = stateProps.current_user ? stateProps.current_user.uid : null;
    const user_email = stateProps.current_user ? stateProps.current_user.email : null;
    return {
        ...stateProps,
        ...dispatchProps,
        ...ownProps,
        setNewPostFormVisibility: (is_visible) =>
            dispatch(setNewPostFormVisibility(is_visible)),
        syncNewPostTitle: (title) =>
            dispatch(syncNewPostTitle(title)),
        syncNewPostContent: (content) =>
            dispatch(syncNewPostContent(content)),
        saveNewPost: (new_post_form) =>
            dispatch(saveNewPost(dispatch,new_post_form, user_email, user_uid)),
    }
}

export default connect(
    mapStateToProps,
    null,
    mergeProps
)(PostForm);

