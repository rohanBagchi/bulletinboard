import {createAction} from 'redux-actions';
import keyMirror from 'keymirror';
import {firebase_ref as firebase} from './index.js';

export const UsersActionTypes = keyMirror({
    SYNC_LOGIN_EMAIL: null,
    SYNC_LOGIN_PASSWORD: null,
	SET_LOGGED_IN_USER: null,
    ATTEMPT_LOG_IN: null,
    ATTEMPT_LOG_OUT: null,
    HANDLE_LOG_IN_ERROR: null,
    HANDLE_LOG_OUT_ERROR: null,
    RESET_LOG_IN_ERROR: null,
    RESET_LOG_IN_FORM: null
});

export const syncLoginEmail = createAction(
    UsersActionTypes.SYNC_LOGIN_EMAIL,
    email => email
);

export const syncLoginPassword = createAction(
    UsersActionTypes.SYNC_LOGIN_PASSWORD,
    password => password
);

export const setLoggedInUser = createAction(
    UsersActionTypes.SET_LOGGED_IN_USER,
    user => ({user})
);

export const resetLoginError = createAction(
    UsersActionTypes.RESET_LOG_IN_ERROR,
    () => ({})
);

export const resetLoginForm = createAction(
	UsersActionTypes.RESET_LOG_IN_FORM,
	() => ({})
);

export const attemptLogin = createAction(
    UsersActionTypes.ATTEMPT_LOG_IN,
    (dispatch, {email, password}) => {
        firebase.auth()
            .signInWithEmailAndPassword(email, password)
            .catch(function(error) {
                dispatch(handleLoginError(error));
            });
    }
);

export const attemptLogout = createAction(
    UsersActionTypes.ATTEMPT_LOG_OUT,
    (dispatch) => {
        firebase.auth().signOut().then(function() {
            dispatch(setLoggedInUser(null));
        }).catch(function(error) {
            dispatch(handleLogoutError(error));
        });
    }
);

export const handleLoginError = createAction(
    UsersActionTypes.HANDLE_LOG_IN_ERROR,
    error => {
        const {code, message} = error;
        return {code, message};
    }
);

export const handleLogoutError = createAction(
    UsersActionTypes.HANDLE_LOG_OUT_ERROR,
    error => {
        const {code, message} = error;
        return {code, message};
    }
);

const default_error_state = {
    code: '',
    message: ''
};

const default_state = {
    users: [],
    login_form: {
        email: '',
        password: '',
        has_error: false,
        errors: default_error_state
    },
    has_logout_error: false,
    logout_errors: default_error_state,
    current_user: null,
    is_current_user_logged_in: true
};

export default function reducer(state, action) {
	state = state || default_state;
	switch(action.type) {
        case UsersActionTypes.SYNC_LOGIN_EMAIL:
            return  {
                ...state,
                login_form: {
                    ...state.login_form,
                    email: action.payload
                }
            };

        case UsersActionTypes.SYNC_LOGIN_PASSWORD:
            return  {
                ...state,
                login_form: {
                    ...state.login_form,
                    password: action.payload
                }
            };

        case UsersActionTypes.SET_LOGGED_IN_USER:
            return  {
                ...state,
                current_user: action.payload.user,
                is_current_user_logged_in: !!action.payload.user
            };

        case UsersActionTypes.HANDLE_LOG_IN_ERROR:
            return {
                ...state,
                login_form: {
                    ...state.login_form,
                    has_error: true,
                    errors: {
                        code: action.payload.code,
                        message: action.payload.message
                    }
                }
            };

        case UsersActionTypes.HANDLE_LOG_OUT_ERROR:
            return {
                ...state,
                has_logout_error: true,
                logout_errors: {
                    ...state.logout_errors,
                    errors: {
                        code: action.payload.code,
                        message: action.payload.message
                    }
                }
            };

        case UsersActionTypes.RESET_LOG_IN_ERROR:
            return {
                ...state,
                login_form: {
                    ...state.login_form,
                    has_error: false,
                    errors: default_error_state
                }
            };

        case UsersActionTypes.RESET_LOG_IN_FORM:
            return {
                ...state,
                login_form: {
                    ...state.login_form,
                    email: '',
                    password: ''
                }
            };
		default:
			return state;
	}
}
