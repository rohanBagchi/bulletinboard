import {combineReducers} from 'redux';
import PostsReducer from './PostsDuck.js';
import UsersDuck from './UsersDuck.js';

export default combineReducers({
    posts_reducer: PostsReducer,
    users_reducer: UsersDuck
});
