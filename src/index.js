import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import store from './store.js';
import AppContainer from './containers/AppContainer.js';
import firebase from 'firebase';
import {setLoggedInUser, resetLoginError, resetLoginForm} from './UsersDuck.js';
import {setPosts, setIsLoading, setUserReadList} from './PostsDuck.js';

// Initialize Firebase
const config = {
    apiKey: "AIzaSyAmgE4Yhxg5KwdHHL0QCXJu6BX39NItVSU",
    authDomain: "bulletin-board-7de92.firebaseapp.com",
    databaseURL: "https://bulletin-board-7de92.firebaseio.com",
    projectId: "bulletin-board-7de92",
    storageBucket: "bulletin-board-7de92.appspot.com",
    messagingSenderId: "320998360608"
};
firebase.initializeApp(config);

export const firebase_ref = firebase;
export const database = firebase.database();

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        store.dispatch(resetLoginError());
        store.dispatch(resetLoginForm());
        setupDBEventHandlers();
    }
    store.dispatch(setLoggedInUser(user));
});

function setupDBEventHandlers(argument) {
    database.ref('/posts').on('value', snap => {
        const posts = snap.val();
        store.dispatch(setPosts(posts));
        store.dispatch(setIsLoading(false));
    });

    database.ref('/read_list').on('value', snap => {
        const read_list = snap.val();
        store.dispatch(setUserReadList(read_list));
    });
}

ReactDOM.render(
    <Provider store={store}>
        <AppContainer />
    </Provider>,
    document.getElementById('root')
);
