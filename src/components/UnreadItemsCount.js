import React from 'react';
import PropTypes from 'prop-types';
import _ from 'underscore';
import {getPostIdsFromReadList} from '../utils/PostsUtils.js';

const UnreadItemsCount = ({post_ids, user_read_list}) => {
    const post_ids_in_read_list = getPostIdsFromReadList(user_read_list);
    const diff = _.difference(post_ids, post_ids_in_read_list);

    if (diff.length === 0) return null;

    return (
        <div className="alert alert-info" role="alert">
            <strong>Heads up! </strong>
            You have {diff.length} unread items.
        </div>
    );
};

UnreadItemsCount.propTypes = {
    post_ids: PropTypes.arrayOf(PropTypes.string),
    user_read_list: PropTypes.object
}

export default UnreadItemsCount;
