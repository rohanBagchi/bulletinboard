import React from 'react';
import PropTypes from 'prop-types';

const PostForm = ({new_post_form, syncNewPostTitle, syncNewPostContent, saveNewPost, setNewPostFormVisibility}) => {
    const handleSave = e => {
        e.preventDefault();
        saveNewPost(new_post_form);
    };

    return (
        <fieldset>
            <legend>
                Add a Post
            </legend>
            <form onSubmit={handleSave} className="post__post-form">
                <div className="row post__post-form__input">
                    <div className="col-md-12">
                        <input
                            type="text"
                            className="form-control"
                            onChange={e => syncNewPostTitle(e.target.value)}
                            value={new_post_form.title}
                            placeholder="Enter title"
                            autoFocus={true}/>
                    </div>
                </div>

                <div className="row post__post-form__input">
                    <div className="col-md-12">
                        <textarea
                            type="text"
                            className="form-control"
                            onChange={e => syncNewPostContent(e.target.value)}
                            value={new_post_form.content}
                            placeholder="Enter post content"
                            maxLength={100}
                        />
                        <span id="helpBlock" className="help-block">
                            Please finish your post within 100 characters
                        </span>
                    </div>
                </div>


                <div className="row post__post-form__form-btn">
                    <div className="col-md-12">
                        <button className="btn btn-primary">
                            Save
                        </button>

                        <button type="button"
                            className="btn btn-default post__post-form__form-btn__cancel-btn"
                            onClick={() => setNewPostFormVisibility(false)}>
                            Cancel
                        </button>
                    </div>
                </div>
            </form>
        </fieldset>
    );
};

PostForm.propTypes = {
    new_post_form: PropTypes.shape({
        title: PropTypes.string,
        content: PropTypes.string
    }).isRequired,
    syncNewPostTitle: PropTypes.func,
    setNewPostFormVisibility: PropTypes.func,
    saveNewPostContent: PropTypes.func
};

export default PostForm;
