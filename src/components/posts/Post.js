import React from 'react';
import PropTypes from 'prop-types';
import Classname from 'classnames';
import {isPostInReadList} from '../../utils/PostsUtils.js';

const Post = ({post, new_post_visible, togglePostExpand, deletePost, user_read_list, current_user_email}) => {
    const post_classes = Classname('post', {
        'post--fade-out': new_post_visible,
        'post--expanded': post.is_expanded
    });

    const read_more_toggle_text = post.is_expanded ? 'Read less' : 'Read more >>';
    const is_post_in_read_list = isPostInReadList(post._id, user_read_list);
    const is_current_user_owner_of_post = post.created_by === current_user_email;
    const post_creator = post.created_by.substring(0, post.created_by.indexOf('@'));

    const handlePostReadmore = () => {
        if (new_post_visible) return;
        togglePostExpand(post._id, post.is_expanded);
    };

    const handleDeletePost = () => {
        if (new_post_visible) return;
        deletePost(post);
    };

    return (
        <div className={post_classes}>
            <div className="row">
                <div className="col-md-12">
                    <div className="post__content">
                        {post.title}
                    </div>
                    {!is_post_in_read_list &&
                        <div className="post__status text-muted pull-right">
                            <span className="label label-info">Unread</span>
                        </div>
                    }
                </div>

            </div>

            <div className="row">
                <div className="col-md-12">
                    <div className="post__author text-muted" title={post.created_by}>
                        by {post_creator}
                    </div>
                    {is_current_user_owner_of_post &&
                        <div className="post__delete">
                            <button
                                className="btn btn-link post__delete__btn"
                                title="Delete Post"
                                onClick={handleDeletePost}>
                                <i className="glyphicon glyphicon-trash"/>
                            </button>
                        </div>
                    }
                    <div className="post__read-more-link pull-right">
                        <button className="btn btn-link post__read-more-link__btn"
                            onClick={handlePostReadmore}>
                            {read_more_toggle_text}
                        </button>
                    </div>
                </div>
            </div>

            {post.is_expanded &&
                <div className="row">
                    <div className="col-md-12">
                        {post.content}
                    </div>
                </div>
            }
        </div>
    );
};

Post.propTypes = {
    post: PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        is_expanded: PropTypes.bool.isRequired
    }).isRequired,
    current_user_email: PropTypes.string,
    new_post_visible: PropTypes.bool,
    user_read_list: PropTypes.object,
    togglePostExpand: PropTypes.func,
    deletePost: PropTypes.func
};

export default Post;
