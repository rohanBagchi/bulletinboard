import React, { Component } from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/App.css';
import Post from './posts/Post.js';
import PostFormContainer from '../containers/PostFormContainer.js';
import LogoutErrorContainer from '../containers/LogoutErrorContainer.js';
import NavbarContainer from '../containers/NavbarContainer.js';
import LoginContainer from '../containers/LoginContainer.js';
import UnreadItemsCountContainer from '../containers/UnreadItemsCountContainer.js';

class App extends Component {
    static propTypes = {
        current_user: PropTypes.shape({
            email: PropTypes.string.isRequired
        }),
        is_logged_in: PropTypes.bool,
        initializePosts: PropTypes.func,
        setNewPostFormVisibility: PropTypes.func,
        syncNewPostTitle: PropTypes.func,
        syncNewPostContent: PropTypes.func,
        togglePostExpand: PropTypes.func,
        deletePost: PropTypes.func,
        posts: PropTypes.array,
        posts_is_loading: PropTypes.bool,
        new_post_form: PropTypes.shape({
            is_visible: PropTypes.bool,
            title: PropTypes.string,
            content: PropTypes.string
        }).isRequired,
        user_read_list: PropTypes.object
    };

    constructor(props) {
        super(props);

        this.renderPosts = this.renderPosts.bind(this);
        this.renderNewPostForm = this.renderNewPostForm.bind(this);
        this.renderAddNewPostBtn = this.renderAddNewPostBtn.bind(this);
    }

    componentWillMount() {
        this.props.initializePosts();
    }

    renderPosts() {
        if (this.props.posts_is_loading) {
            return (
                <div>
                    Loading posts...
                </div>
            );
        }
        return this.props.posts && this.props.posts.map((post, index) => {
            return (
                <Post
                    key={index}
                    post={post}
                    current_user_email={this.props.current_user.email}
                    user_read_list={this.props.user_read_list}
                    new_post_visible={this.props.new_post_form.is_visible}
                    togglePostExpand={this.props.togglePostExpand}
                    deletePost={this.props.deletePost}
                />
            );
        })
    }

    renderNewPostForm() {
        if (!this.props.new_post_form.is_visible) {
            return null;
        }
        return (
            <PostFormContainer/>
        );
    }

    renderAddNewPostBtn() {
        if (this.props.new_post_form.is_visible) {
            return null;
        }
        return (
            <button className="btn btn-default"
                disabled={this.props.posts_is_loading}
                onClick={() => this.props.setNewPostFormVisibility(true)}>
                Add
            </button>
        );
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <NavbarContainer/>
                    {!this.props.is_logged_in &&
                        <LoginContainer/>
                    }

                    {this.props.is_logged_in &&
                        <div className="col-md-12">
                            <div className="posts">
                                <LogoutErrorContainer/>
                                <UnreadItemsCountContainer/>
                                {this.renderAddNewPostBtn()}
                                {this.renderNewPostForm()}
                                <div className="posts__feed">
                                    {this.renderPosts()}
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export default App;
