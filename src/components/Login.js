import React from 'react';
import PropTypes from 'prop-types';

const Login = ({email, password, has_error, errors, syncLoginEmail, syncLoginPassword, attemptLogin}) => {
    const handleLogin = e => {
        e.preventDefault();
        attemptLogin({email, password})
    };

    return (
        <fieldset className="login-page">
            <legend>
                Login
            </legend>
            {has_error &&
                <div className="alert alert-danger">
                    <strong>{errors.code}: </strong>
                    {errors.message}
                </div>
            }
            <form onSubmit={handleLogin} className="login-form">
                <div className="row post__post-form__input">
                    <div className="col-md-12">
                        <input
                            type="text"
                            className="form-control"
                            onChange={e => syncLoginEmail(e.target.value)}
                            value={email}
                            placeholder="Email"
                            autoFocus={true}/>
                    </div>
                </div>

                <div className="row post__post-form__input">
                    <div className="col-md-12">
                        <input
                            type="password"
                            className="form-control"
                            onChange={e => syncLoginPassword(e.target.value)}
                            value={password}
                            placeholder="Password"/>
                    </div>
                </div>


                <div className="row post__post-form__form-btn">
                    <div className="col-md-12">
                        <button className="btn btn-primary">
                            Login
                        </button>
                    </div>
                </div>
            </form>
        </fieldset>
    );
};

Login.propTypes = {
    email: PropTypes.string,
    password: PropTypes.string,
    has_error: PropTypes.bool,
    errors: PropTypes.shape({
        code: PropTypes.string,
        message: PropTypes.string
    }),
    syncLoginEmail: PropTypes.func.isRequired,
    syncLoginPassword: PropTypes.func.isRequired,
    attemptLogin: PropTypes.func.isRequired
};

export default Login;
