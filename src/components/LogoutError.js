import React from 'react';
import PropTypes from 'prop-types';

const LogoutError = ({has_logout_error, logout_errors}) => {
    if (!has_logout_error) return null;

    return (
        <div className="alert alert-danger">
            <strong>{logout_errors.code}: </strong>
            {logout_errors.message}
        </div>
    );
};

LogoutError.propTypes = {
    has_logout_error: PropTypes.bool,
    logout_errors: PropTypes.shape({
        code: PropTypes.string,
        message: PropTypes.string
    })
};

export default LogoutError;
