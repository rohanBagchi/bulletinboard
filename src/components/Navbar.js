import React from 'react';
import PropTypes from 'prop-types';

const Navbar = ({is_logged_in, attemptLogout}) => {
    return (
        <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container">
                <div className="posts__navbar">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">
                            Bulletin Board
                        </a>
                    </div>

                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            {is_logged_in &&
                                <li>
                                    <button className="btn btn-link navbar-btn" onClick={attemptLogout}>
                                        <i className="glyphicon glyphicon-off"/> Logout
                                    </button>
                                </li>
                            }
                        </ul>
                    </div>

                </div>
            </div>
        </nav>
    );
};

Navbar.propTypes = {
    is_logged_in: PropTypes.bool,
    attemptLogout: PropTypes.func
};

export default Navbar;
